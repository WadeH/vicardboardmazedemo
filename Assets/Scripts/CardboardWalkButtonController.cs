﻿using System;
using UnityEngine;
using System.Collections;
using UnityStandardAssets.CrossPlatformInput;


public class CardboardWalkButtonController : MonoBehaviour
{
	#region [Parameters]

	[Tooltip ("Cause debug information to be written to the console")]
	public Boolean debugInfo = false;
	// Name of input axis; used if emulateAxis is true
	[Tooltip ("Name of input axis to use for movement")]
	public String axisName = "Vertical";
	private Cardboard cardboardObject = null;

	#endregion

	// Use this for initialization
	void Start ()
	{

		#if UNITY_ANDROID || UNITY_IOS
		try {
			cardboardObject = Cardboard.SDK;
		} catch (Exception ex) {
			Debug.logger.Log ("Exception occured while retrieving Cardboard object");
			if (debugInfo) {
				Debug.logger.Log (
					String.Format ("Exception Type: {0}, Location: {1}, Message: {2}", 
						ex.GetType ().ToString (), ex.Source, ex.Message));
			}
		}
		#endif

		if (cardboardObject != null) {
			cardboardObject.OnTrigger += OnCardboardTrigger;
		}
		//#endif

	}

	// Update is called once per frame
	void Update ()
	{
		// reset state of trigger
		EmulateButton (false);
	}

	/// <summary>
	/// Handles the Cardboard trigger event, and emulates pressing the specified key
	/// </summary>
	public void OnCardboardTrigger ()
	{
		if (this.debugInfo) {
			Debug.logger.Log (String.Format ("Cardboard Trigger Event > State: {0}", 
				this.cardboardObject.Triggered.ToString ()));
		}
		EmulateButton (cardboardObject.Triggered);
	}

	/// <summary>
	/// Emulates the press of button specified by walkButtonName variable
	/// </summary>
	/// <param name="pressed">If set to <c>true</c>, trigger ButtonDown, 
	/// else ButtonUp.</param>
	private void EmulateButton (bool pressed)
	{
		// Prevent these events from firing 
		#if UNITY_ANDROID || UNITY_IOS
		if (pressed) {
			//if (emulateAxis) {
				CrossPlatformInputManager.SetAxisPositive (axisName);
			/*} else {
				CrossPlatformInputManager.SetButtonDown (walkButtonName);
			}*/
				
		} else {
			//if (emulateAxis) {
				//CrossPlatformInputManager.SetAxisZero (axisName);
				// Try to arrest movement
				float rate=CrossPlatformInputManager.GetAxis(axisName);
				float cutRate = Math.Max(rate*0.5f,0f);
				// If cut rate is less than current input, update it
				if (cutRate < rate) {
					CrossPlatformInputManager.SetAxis (axisName, cutRate); 
				}
			/*} else {
				CrossPlatformInputManager.SetButtonUp (walkButtonName);
			}*/
		}
		#endif
	}
}
