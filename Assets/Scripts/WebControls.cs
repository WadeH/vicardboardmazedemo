﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Escape/U Unlocks mouse cursor lock
/// L Locks mouse cursor lock
/// </summary>
public class WebControls : MonoBehaviour {

	UnityEngine.UI.Text text;
	CursorLockMode state;

	// Use this for initialization
	void Start () {
		this.text = (UnityEngine.UI.Text)GetComponent(typeof(UnityEngine.UI.Text));
		this.state = Cursor.lockState;
	}
		
	// Update is called once per frame
	void Update () {
		switch (this.state) {
		case CursorLockMode.Locked:
			this.text.text = "Press 'U' to release mouse cursor";
			break;
		case CursorLockMode.None:
			this.text.text = "Press 'L' to lock mouse cursor";
			break;
		}
		if (Input.GetKeyDown(KeyCode.L)) {
			state = CursorLockMode.Locked;
		}
		if (Input.GetKeyDown (KeyCode.Escape) || Input.GetKeyDown(KeyCode.U)) {
			this.state = CursorLockMode.None;
		}
		if (Cursor.lockState != this.state) {
			Cursor.lockState = this.state;
			if (Cursor.lockState == CursorLockMode.None) {
				Cursor.visible = true;
			}
		}
	}

}
